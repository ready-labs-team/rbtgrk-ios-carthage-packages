# Overview

Contains helper json files for binary Carthage PM compatibility.


## Fabric & Crashlytics 
Link Crashlytics & Fabric frameworks but **do not add them** to the Carthage `copy-frameworks phase`.


## Google Sign-In

(current version for Google Sign-In: 4.2.0)

Add `GoogleSignIn.bundle` to your Xcode project's Copy Bundle Resources build phase.

Add the `rbtgrk_ios_carthage_packages.framework` to the project, as per Carthage instructions.
